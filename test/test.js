var request = require('supertest')
var assert = require('assert')
var mongoose = require('mongoose')

var app = require('../server.js')

var Visit = require ('../visit')

describe('GET /', function() {
  it('displays "Hello World!"', function(done) {
    request(app).get('/').expect('Hello World!', done);
  });

  it ('logs a visit to the database', function(done) {
    Visit.count({}, function(err, count){
      var previous_count = count;
      request(app).get('/').end(function(){
        Visit.count({}, function(err, count){
          assert.equal(previous_count + 1, count);
          done();
        })
      });
    });
  });
});
