var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var visitSchema = new Schema({
  visited_at: Date
});

visitSchema.pre('save', function(next) {
  var currentDate = new Date();
  this.visited_at = currentDate;
  next();
});

var Visit = mongoose.model('Visit', visitSchema);

module.exports = Visit;
